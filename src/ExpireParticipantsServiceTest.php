<?php

declare(strict_types=1);


namespace Rivulent\ExpireParticipants;


use App\Exceptions\InvalidArgumentException;
use App\Exceptions\PropNotFoundException;
use App\Models\ParticipantWindow;
use App\Models\Response;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\DB;
use Spatie\ModelStatus\Exceptions\InvalidStatus;
use stdClass;
use Tests\TestCase;
use Throwable;
use TypeError;

final class ExpireParticipantsServiceTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Status with interval in days before a participant is marked as expired
     *
     * @var array
     */
    private $statusExpiration;

    /**
     * An array of variable names identified as containing contact information that should be deleted
     *
     * @var array
     */
    private $contactInfo;

    /**
     * Build a minor suitable for having a parent
     *
     * @var array
     */
    private $minorParticipantProps = [
        'P1LNAME' => 'Last',
        'P1FNAME' => 'First',
        'P1EMAIL' => 'test@test.com',
        'P1CPHONE' => '555-555-5555',
        'P1HPHONE' => '666-666-6666',
        'P1CALL' => '1',
        'P1CALL.TEXT' => 'Any',
        'P1STREET' => '111 Ruby St',
        'P1ROOM' => 'Rm 1',
        'P1CITY' => 'Ephrata',
        'P1STATE' => '49',
        'P1ZIP' => '98823',
    ];

    protected function setUp(): void
    {
        parent::setUp();

        $this->statusExpiration = config('expire-participants.status_expiration');
        $this->contactInfo = config('expire-participants.contact_info');
    }

    /**
     * @test
     */
    public function get_returns_empty_collection_if_statuses_not_found()
    {
        create('App\Models\Participant', [], 5);

        $this->assertEquals(0, ExpireParticipantsService::get()->count());
    }

    /**
     * @test
     */
    public function get_returns_empty_collection_if_no_expired_found()
    {
        $this->withoutEvents();

        foreach($this->statusExpiration as $oldStatus => $expiration) {
            $participant = create('App\Models\Participant');
            $participant->setStatus($oldStatus, 'testing');
        }

        $this->assertEquals(0, ExpireParticipantsService::get()->count());
    }

    /**
     * @test
     */
    public function get_returns_empty_collection_if_expired_days_equals_limit()
    {
        $this->withoutEvents();

        foreach($this->statusExpiration as $oldStatus => $expiration) {
            $participant = create('App\Models\Participant');
            $participant->setStatus($oldStatus, 'testing');

            DB::table('statuses')
                ->where('id', '=', $participant->status()->id)
                ->update([
                    'created_at' => Carbon::tomorrow()->addDays((-1 * $expiration[0]))
                ]);
        }

        $this->assertEquals(0, ExpireParticipantsService::get()->count());
    }

    /**
     * @test
     */
    public function get_returns_collection_if_expired_one_day_longer_than_limit()
    {
        $this->withoutEvents();

        foreach($this->statusExpiration as $oldStatus => $expiration) {
            $participant = create('App\Models\Participant');
            $participant->setStatus($oldStatus, 'testing');

            DB::table('statuses')
                ->where('id', '=', $participant->status()->id)
                ->update([
                    'created_at' => Carbon::today()->addDays((-1 * ($expiration[0] + 1)))
                ]);
        }

        $this->assertEquals(count($this->statusExpiration), ExpireParticipantsService::get()->count());
    }

    /**
     * @test
     */
    public function get_returns_collection_limited_by_take()
    {
        $this->withoutEvents();

        foreach($this->statusExpiration as $oldStatus => $expiration) {
            $participant = create('App\Models\Participant');
            $participant->setStatus($oldStatus, 'testing');

            DB::table('statuses')
                ->where('id', '=', $participant->status()->id)
                ->update([
                    'created_at' => Carbon::today()->addDays((-1 * ($expiration[0] + 1)))
                ]);
        }

        $this->assertEquals(3, ExpireParticipantsService::get(3)->count());
    }

    /**
     * @test
     */
    public function constructor_throws_if_type_error()
    {
        $this->expectException(TypeError::class);
        new ExpireParticipantsService(new stdClass());
    }

    /**
     * @test
     */
    public function constructor_throws_if_status_not_able_to_expire()
    {
        $this->expectException(InvalidArgumentException::class);
        $participant = create('App\Models\Participant');
        $participant->setStatus('eligible-minor', 'testing');

        new ExpireParticipantsService($participant);
    }

    /**
     * @test
     */
    public function constructor_throws_if_participant_not_expired()
    {
        $this->expectException(InvalidArgumentException::class);
        $participant = create('App\Models\Participant');
        $participant->setStatus('pending', 'testing');

        new ExpireParticipantsService($participant);
    }

    /**
     * @test
     * @dataProvider delete_expired_full
     * @param string $oldStatus
     * @param int $daysInStatus
     * @throws InvalidArgumentException
     * @throws InvalidStatus
     * @throws PropNotFoundException
     * @throws Throwable
     */
    public function delete_expired_deletes_all_participant_records(string $oldStatus, int $daysInStatus)
    {
        $this->withoutEvents();

        $participant = create('App\Models\Participant');
        $participant->setStatus($oldStatus, 'testing');

        DB::table('statuses')
            ->where('id', '=', $participant->status()->id)
            ->update([
                'created_at' => Carbon::today()->addDays((-1 * $daysInStatus))
            ]);

        $window = create('App\Models\Window');
        $pws = create('App\Models\ParticipantWindow', [
            'window_id' => $window->id,
            'participant_id' => $participant->id,
        ], 3);

        foreach ($pws as $pw) {
            create('App\Models\Response', [
                'participant_window_id' => $pw->id,
            ], 3);
        }

        $service = new ExpireParticipantsService($participant);
        $service->deleteExpired();

        $this->assertEquals('expired', $participant->status);
        $this->assertEquals(0, Response::all()->count());
        $this->assertEquals(0, ParticipantWindow::all()->count());
        $this->assertNotNull($participant->deleted_at);
    }

    public function delete_expired_full()
    {
        return [
            ['pending', 35],
            ['ineligible', 35],
            ['declined', 35],
            ['eligible-pending', 43],
            ['parent-pending', 44],
            ['consented-parent', 33],
        ];
    }

    /**
     * @test
     * @dataProvider delete_expired_contact
     * @param string $oldStatus
     * @param int $daysInStatus
     * @throws InvalidArgumentException
     * @throws InvalidStatus
     * @throws PropNotFoundException
     * @throws Throwable
     */
    public function delete_expired_deletes_participant_contact_info(string $oldStatus, int $daysInStatus)
    {
        $this->withoutEvents();

        $participant = create('App\Models\Participant');
        $participant->setStatus($oldStatus, 'testing');

        DB::table('statuses')
            ->where('id', '=', $participant->status()->id)
            ->update([
                'created_at' => Carbon::today()->addDays((-1 * $daysInStatus))
            ]);

        $this->populate_contact_info($participant);

        $service = new ExpireParticipantsService($participant);
        $service->deleteExpired();

        $this->assertEquals('expired-contact', $participant->status);
        $this->assertNull($participant->name);
        $this->assertNull($participant->phone);
        $this->assertEquals(0, $participant->phone_ok);
        $this->assertEquals(0, $participant->vm_ok);
        $this->assertEquals(0, $participant->sms_ok);
        $this->assertNull($participant->email);
        $this->assertEquals(0, $participant->email_ok);
        $this->assertNull($participant->props);
        $this->assertNull($participant->deleted_at);

        foreach ($participant->participantWindows as $pw) {
            $this->assertEquals(3, $pw->responses->count());
        }
    }

    public function delete_expired_contact()
    {
        return [
            ['verified', 42],
            ['adult-reconsent', 44],
        ];
    }

    /**
     * @test
     *
     * @throws InvalidArgumentException
     * @throws InvalidStatus
     * @throws PropNotFoundException
     * @throws Throwable
     */
    public function delete_expired_deletes_contact_info_even_if_future_consent_is_1()
    {
        $this->withoutEvents();

        $participant = create('App\Models\Participant');
        $participant->setStatus('verified', 'testing');
        $participant->setProp('FUTURE', '1');

        DB::table('statuses')
            ->where('id', '=', $participant->status()->id)
            ->update([
                'created_at' => Carbon::today()->addDays(-42)
            ]);

        $this->populate_contact_info($participant);

        $service = new ExpireParticipantsService($participant);
        $service->deleteExpired();

        $this->assertEquals('expired-contact', $participant->status);
        $this->assertNull($participant->name);
        $this->assertNull($participant->phone);
        $this->assertEquals(0, $participant->phone_ok);
        $this->assertEquals(0, $participant->vm_ok);
        $this->assertEquals(0, $participant->sms_ok);
        $this->assertNull($participant->email);
        $this->assertEquals(0, $participant->email_ok);
        $this->assertNull($participant->props);
        $this->assertNull($participant->deleted_at);

        $this->assertEquals(
            9, Response::count()
        );
    }

    /**
     * @test
     *
     * @throws InvalidArgumentException
     * @throws InvalidStatus
     * @throws PropNotFoundException
     * @throws Throwable
     */
    public function delete_expired_deletes_contact_info_minor_if_parent_future_consent_is_0()
    {
        $this->withoutEvents();

        $this->seed('PermissionsTableSeeder');

        $participant = create('App\Models\Participant');
        $participant->user->syncRoles(['participant']);
        $participant->setStatus('minor');

        foreach ($this->minorParticipantProps as $key => $value) {
            $participant->setProp($key, $value);
        }

        $participant->setStatus('eligible-pending');
        $this->populate_future_consent_parent($participant, '0');

        $participant->setStatus('verified', 'testing');
        $participant->setProp('FUTURE', '1');

        DB::table('statuses')
            ->where('id', '=', $participant->status()->id)
            ->update([
                'created_at' => Carbon::today()->addDays(-42)
            ]);

        $this->populate_contact_info($participant);

        $service = new ExpireParticipantsService($participant);
        $service->deleteExpired();

        $this->assertEquals('expired-contact', $participant->status);
        $this->assertNull($participant->name);
        $this->assertNull($participant->phone);
        $this->assertEquals(0, $participant->phone_ok);
        $this->assertEquals(0, $participant->vm_ok);
        $this->assertEquals(0, $participant->sms_ok);
        $this->assertNull($participant->email);
        $this->assertEquals(0, $participant->email_ok);
        $this->assertNull($participant->props);
        $this->assertNull($participant->deleted_at);

        foreach ($participant->participantWindows as $pw) {
            $this->assertEquals(3, $pw->responses->count());
        }
    }

    /**
     * @test
     *
     * @throws InvalidArgumentException
     * @throws InvalidStatus
     * @throws PropNotFoundException
     * @throws Throwable
     */
    public function delete_expired_deletes_contact_info_minor_even_if_parent_and_future_consent_is_1()
    {
        $this->withoutEvents();

        $this->seed('PermissionsTableSeeder');

        $participant = create('App\Models\Participant');
        $participant->user->syncRoles(['participant']);
        $participant->setStatus('minor');

        foreach ($this->minorParticipantProps as $key => $value) {
            $participant->setProp($key, $value);
        }

        $participant->setStatus('eligible-pending');
        $this->populate_future_consent_parent($participant);

        $participant->setStatus('verified', 'testing');
        $participant->setProp('FUTURE', '1');

        DB::table('statuses')
            ->where('id', '=', $participant->status()->id)
            ->update([
                'created_at' => Carbon::today()->addDays(-42)
            ]);

        $this->populate_contact_info($participant);

        $service = new ExpireParticipantsService($participant);
        $service->deleteExpired();

        $this->assertEquals('expired-contact', $participant->status);
        $this->assertNull($participant->name);
        $this->assertNull($participant->phone);
        $this->assertEquals(0, $participant->phone_ok);
        $this->assertEquals(0, $participant->vm_ok);
        $this->assertEquals(0, $participant->sms_ok);
        $this->assertNull($participant->email);
        $this->assertEquals(0, $participant->email_ok);
        $this->assertNull($participant->props);
        $this->assertNull($participant->deleted_at);

        $this->assertEquals(
            9, Response::count()
        );
    }

    private function populate_contact_info($participant)
    {
        $survey = create('App\Models\Survey');
        $window = create('App\Models\Window');
        $pws = create('App\Models\ParticipantWindow', [
            'window_id' => $window->id,
            'participant_id' => $participant->id,
        ], 3);

        foreach ($pws as $pw) {
            create('App\Models\Response', [
                'participant_window_id' => $pw->id,
                'element_id' => (999 + $pw->id),
            ], 3);
        }

        foreach ($this->contactInfo as $variableName) {
            $collection = create('App\Models\Collection', [
                'survey_id' => $survey->id,
            ]);
            $element = create('App\Models\Element', [
                'collection_id' =>  $collection->id,
                'var_name' => $variableName,
                'type' => 'text',
            ]);
            $answer = create('App\Models\Answer', [
                'element_id' => $element->id,
                'type' => 'text',
                'var_name' => null,
            ]);

            create('App\Models\Response', [
                'participant_window_id' => $pws->random()->id,
                'element_id' => $element->id,
                'answer_id' => $answer->id,
            ]);
        }
    }

    private function populate_future_consent_parent($participant, $pFuture = '1')
    {
        $parent = (new CreateParentForParticipantService($participant))->make('P1');

        $parent->setProp('P_FUTURE', $pFuture);
    }
}