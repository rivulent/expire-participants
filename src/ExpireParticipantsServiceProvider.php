<?php

namespace Rivulent\ExpireParticipants;

use Illuminate\Support\ServiceProvider;

class ExpireParticipantsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/expire-participants.php', 'expire-participants'
        );

        if ($this->app->runningInConsole()) {
            $this->commands([
                ExpireParticipantsCommand::class,
            ]);
        }

        $this->publishes([
            __DIR__ . '/expire-participants.php' => config_path('expire-participants.php')
        ], 'config');
    }
}
