<?php

declare(strict_types=1);


namespace Rivulent\ExpireParticipants;


use App\Exceptions\InvalidArgumentException;
use App\Exceptions\PropNotFoundException;
use App\Models\Element;
use App\Models\Participant;
use App\Models\Response;
use Spatie\ModelStatus\Status;
use Carbon\Carbon;
use Spatie\ModelStatus\Exceptions\InvalidStatus;
use Throwable;

final class ExpireParticipantsService
{
    /**
     * An array of variable names identified as containing contact information that should be deleted
     *
     * @var array
     */
    private $contactInfo;

    private Participant $participant;

    /**
     * The result of setExpiration for $this->participant
     *
     * @var array
     */
    private $expiration = [];

    /**
     * ExpireParticipantsService constructor.
     * @param Participant $participant
     * @throws InvalidArgumentException
     * @throws ParticipantNotExpiredException
     */
    public function __construct(Participant $participant)
    {
        $this->contactInfo = config('expire-participants.contact_info');
        $this->participant = $participant;

        $this->expiration = self::calculateExpiration($this->participant->status());

        if ($this->expiration['expired'] === false) {
            throw new InvalidArgumentException('Participant is not expired');
        }

    }

    public function deleteExpired(): bool
    {
        if ($this->participant === null) {
            throw new InvalidArgumentException('Participant must be set in the constructor');
        }

        if ($this->expiration['newStatus'] === 'expired-contact') {
            return $this->deleteExpireParticipantContactInfoOnly();
        }

        if ($this->expiration['newStatus'] === 'expired') {
            return $this->deleteExpireParticipantFull();
        }
    }

    public static function calculateExpiration(Status $status): array
    {
        $statusExpiration = config('expire-participants.status_expiration');
        
        if (!in_array($status->name, array_keys($statusExpiration))) {
            throw new ParticipantNotExpiredException('Participant status is not eligible for expiration');
        }

        $tomorrow = Carbon::tomorrow(config('app.project_timezone'));

        $days = $status
            ->created_at
            ->diffInDays($tomorrow);

        return [
            'oldStatus' => $status->name,
            'newStatus' => $statusExpiration[$status->name][1],
            'days' => $days,
            'expired' => $days > $statusExpiration[$status->name][0],
        ];
    }

    private function deleteExpireParticipantFull(): bool
    {
        $this->updateParticipantStatus();

        $this->deleteAllResponses();

        $this->deleteParticipantWindows();

        $this->deleteParticipantFull();

        return true;
    }

    private function deleteExpireParticipantContactInfoOnly(): bool
    {
        $this->updateParticipantStatus();

        $this->deleteContactInfoResponses();

        $this->deleteParticipantContactInfo();

        return true;
    }

    private function deleteAllResponses(): void
    {
        $this->participant->responses->each(function ($item) {
            $item->delete();
        });
    }

    private function deleteContactInfoResponses(): void
    {
        $participantWindowIds = $this->participant
            ->participantWindows()
            ->get(['id'])
            ->map(function ($item) {
                return $item->id;
            })
            ->toArray();

        $elementIds = Element
            ::whereIn('var_name', $this->contactInfo)
            ->get(['id'])
            ->map(function ($item) {
                return $item->id;
            })
            ->toArray();

        Response
            ::whereIn('participant_window_id', $participantWindowIds)
            ->whereIn('element_id', $elementIds)
            ->get()
            ->each(function ($item) {
                $item->delete();
            });
    }

    private function deleteParticipantContactInfo(): void
    {
        $participant = Participant::findOrFail($this->participant->id);

        $participant->name = null;
        $participant->phone = null;
        $participant->phone_ok = 0;
        $participant->vm_ok = 0;
        $participant->sms_ok = 0;
        $participant->email = null;
        $participant->email_ok = 0;
        $participant->props = null;

        $participant->save();

        $this->participant->refresh();
    }

    private function deleteParticipantFull(): void
    {
        $this->participant->delete();
    }

    private function deleteParticipantWindows(): void
    {
        $this->participant->participantWindows->each(function ($item) {
            $item->delete();
        });
    }

    private function updateParticipantStatus(): void
    {
        $reason = $this->expiration['days'] . ' days in status ' . $this->expiration['oldStatus'];

        $this->participant->setStatus($this->expiration['newStatus'], $reason);
    }
}