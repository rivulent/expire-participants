<?php
/**
 * Created by PhpStorm.
 * User: cjmcd
 * Date: 10/12/2018
 * Time: 7:58 AM
 */
declare(strict_types=1);


namespace Rivulent\ExpireParticipants;


use App\Models\Participant;
use Illuminate\Database\Eloquent\Collection;

final class ExpireParticipantsQuery
{
    public static function run(int $take = null): Collection
    {
        $statuses = array_keys(config('expire-participants.status_expiration'));

        return Participant
            ::currentStatus($statuses)
            ->when($take, function ($query, $take) {
                return $query->take($take);
            })
            ->get()
            ->transform(function ($item) {
                $item->expiration = ExpireParticipantsService::calculateExpiration($item->status());

                return $item;
            })
            ->filter(function ($item) {
                return $item->expiration['expired'];
            });
    }
}