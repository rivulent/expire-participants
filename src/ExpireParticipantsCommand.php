<?php

namespace Rivulent\ExpireParticipants;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ExpireParticipantsCommand extends Command
{
    protected $signature = 'participant:expire {--take= : The number of participants to expire}';

    protected $description = 'Expire participants whose status has been stalled for the specified time';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $take = is_numeric($this->option('take')) ? (int) $this->option('take') : null;
        $participants = (new ExpireParticipantsQuery())->run($take);

        $msg = 'Selected ' . $participants->count() . ' participants to mark as expired.';
        $this->line($msg);
        Log::debug($msg);

        $participants
            ->each(function ($item) {
                try {
                    (new ExpireParticipantsService($item))->deleteExpired();

                    $msg = 'Successfully expired participant ' . $item->id;
                    $this->line($msg);
                    Log::debug($msg);
                } catch (\Exception $e) {
                    $msg = 'Failed to expire participant ' . $item->id;
                    $this->line($msg);
                    Log::critical($msg, [
                        'participant_id' => $item->id,
                        'message' => $e->getMessage(),
                    ]);
                }
            });
    }
}