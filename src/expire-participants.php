<?php

return [

    /*
     * Status with interval in days before a participant is marked as expired
     *
     */
    'status_expiration' => [
        'pending' => [30, 'expired'],
        'ineligible' => [30, 'expired'],
        'declined' => [30, 'expired'],
        'eligible-pending' => [41, 'expired'],
        'verified' => [41, 'expired-contact'],
    ],

    /*
     * An array of variable names identified as containing contact information that should be deleted
     *
     */
    'contact_info' => [
        'FIRSTNAME', 'LASTNAME', 'LOCPHONE', 'CELL', 'STREET', 'APT', 'CITY', 'ZIPCODE',
    ],
];
